# Excel像素画生成器

#### 介绍
Excel像素画（网上的例子）：
![网络案例](https://gitee.com/uploads/images/2019/0430/140011_9202dcc4_1342689.jpeg "timg.jpg")
即：将一张Excel表的所有单元格都设置成正方形，然后逐个填充颜色，最终组成一幅画。本工具可读取指定的图片，遍历该图片的每个像素点，获取RGB值后，自动向Excel表格的相应单元格内填充同样的颜色，使之成为一副像素画。

生成器使用效果图：


 **原图1** ![原图1](https://gitee.com/uploads/images/2019/0430/140720_170e97a0_1342689.jpeg "a.jpg")

 **成品1** ![成品1](https://gitee.com/uploads/images/2019/0430/140821_e9b5b5dd_1342689.jpeg "a1.jpg")

 **细节图1** ![细节图1](https://gitee.com/uploads/images/2019/0430/140845_43a7193e_1342689.jpeg "a2.jpg")

 **原图2** ![原图2](https://gitee.com/uploads/images/2019/0430/141002_f7d1c6bc_1342689.jpeg "b.jpg")

 **成品2** ![成品2](https://gitee.com/uploads/images/2019/0430/141018_370afe83_1342689.jpeg "b1.jpg")

 **细节图2** ![细节图2](https://gitee.com/uploads/images/2019/0430/141050_3a2d9fba_1342689.jpeg "b2.jpg")

 **原图3** ![原图3](https://gitee.com/uploads/images/2019/0430/141137_65198975_1342689.jpeg "c.jpg")

 **成品3** ![成品3](https://gitee.com/uploads/images/2019/0430/141157_0db5b29d_1342689.jpeg "c1.jpg")

 **细节图3** ![细节图3](https://gitee.com/uploads/images/2019/0430/141212_f7644a00_1342689.jpeg "c2.jpg")


#### 软件架构
Java 8


#### 使用说明
![启动方式](https://gitee.com/uploads/images/2019/0430/141434_df1273bc_1342689.jpeg "QQ截图20190430141421.jpg")
启动方式：命令行启动，第一个参数为源图片路径，第二个参数为输出Excel的路径

#### 使用说明
1、本项目依赖Apache POI；
2、原图片支持jpg格式和png格式；
3、项目为单线程，当图片很大时，速度很慢。有兴趣的朋友请自行改成多线程。