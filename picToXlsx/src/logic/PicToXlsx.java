package logic;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Excel像素画生成器.
 * @author Rex
 *
 */
public class PicToXlsx {
	
	public static void main(String[] args) throws Exception {
		// 参数检查
		if (args.length != 2) {
			throw new Exception("参数数量有误");
		}
		if (args[1].endsWith(".xlsx") == false) {
			throw new Exception("输出路径必须是xlsx文件");
		}
		
		// list的容量为图片高度（行数），rgb数组的长度为图片宽度（列数）
		List<int[]> load = loadPic(args[0]);
		// 填入到Excel
		createXlsx(load, args[1]);
	}

	/**
	 * 生成成品Excel.
	 * @param list
	 * @param path
	 * @throws Exception
	 */
	private static void createXlsx(List<int[]> list, String path) throws Exception {
		// 先保存一个空Excel文件，在磁盘占位
		FileOutputStream out = new FileOutputStream(path);
		XSSFWorkbook excel = new XSSFWorkbook();
		excel.createSheet("myLove");
		excel.write(out);
		excel.close();
		out.close();

		int size = list.size();
		int columns = size - 1;
		int length = list.get(0).length;
		int rows = length - 1;
		
		// 读取占位的文件，设置列宽
		excel = new XSSFWorkbook(new FileInputStream(path));
		XSSFSheet sht = excel.getSheetAt(0);
		for (int j = 0; j < length; j++) {
			sht.setColumnWidth(j, (short) 500);
		}
		
		// 开始按行填充颜色
		for (int i = 0; i < size; i++) {
			System.out.println(i);// 提示行数
			int[] rgbs = list.get(i);
			Row row = sht.createRow(i);
			// 设置行高，使之与列宽相等（即：设置单元格为正方形）
			row.setHeight((short) 250);
			for (int j = 0; j < length; j++) {
				XSSFCellStyle style = excel.createCellStyle();
				style.setFillForegroundColor(new XSSFColor(new Color(rgbs[j])));
				style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				row.createCell(j).setCellStyle(style);
				// 填充完最后一格后，重新写出文件
				if (i == columns && j == rows) {
					out = new FileOutputStream(path);
					excel.write(out);
					excel.close();
					out.close();
					System.out.println("done");// 提示完成
				}
			}
		}
	}

	/**
	 * 加载图片，获取像素点RGB信息.
	 * @param path
	 * @return
	 * @throws Exception
	 */
	private static List<int[]> loadPic(String path) throws Exception {
		List<int[]> pic = new ArrayList<>();

		BufferedImage bi = ImageIO.read(new File(path));

		int width = bi.getWidth();
		int height = bi.getHeight();
		int minx = bi.getMinX();
		int miny = bi.getMinY();
		int size = width - minx;

		// 按行遍历像素点
		for (int j = miny; j < height; j++) {
			int[] array = new int[size];
			int index = 0;
			for (int i = minx; i < width; i++) {
				array[index] = bi.getRGB(i, j);
				index++;
			}
			pic.add(array);
		}

		return pic;
	}
	
}